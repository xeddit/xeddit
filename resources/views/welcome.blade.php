<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Xeddit Private Browser</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}"/>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#fe4201">
    <meta name="msapplication-TileColor" content="#efefef">
    <meta name="theme-color" content="#efefef">
</head>
<body>
<div class="container-fluid pb-5 mb-5" id="app">
    <reddit-index></reddit-index>
    <div class="row justify-content-center mt-5">
        <div class="col-10 text-center">
            <a href="https://gitlab.com/xeddit/xeddit/" class="btn btn-outline-secondary m-4" target="_blank"><i class="fa fa-gitlab"></i> Fork me</a>
            <a href="mailto:xeddit@tuta.io" class="btn btn-outline-primary"><i class="fa fa-envelope"></i> xeddit@tuta.io</a>
        </div>
    </div>
</div>
</body>
<script async src="{{ mix('js/app.js') }}" type="text/javascript" async defer></script>
</html>